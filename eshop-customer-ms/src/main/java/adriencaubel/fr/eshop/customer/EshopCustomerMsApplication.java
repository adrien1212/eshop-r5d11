package adriencaubel.fr.eshop.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EshopCustomerMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EshopCustomerMsApplication.class, args);
	}

}
