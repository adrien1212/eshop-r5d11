package adriencaubel.fr.eshop.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EshopOrderMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EshopOrderMsApplication.class, args);
	}

}
