package adriencaubel.fr.eshop.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EshopProductMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EshopProductMsApplication.class, args);
	}

}
